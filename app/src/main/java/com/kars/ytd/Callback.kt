package com.kars.ytd

interface Callback {

    fun onSuccess()
    fun onError()

}

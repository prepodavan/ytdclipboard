package com.kars.ytd.net

import android.content.Context
import android.util.Log
import com.android.volley.DefaultRetryPolicy
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.kars.ytd.Callback
import com.kars.ytd.LogTag
import com.kars.ytd.RemoteMusicObject
import java.lang.Exception

class PrepareMusicRequest(private val mo: RemoteMusicObject, private val context: Context) : IAsyncRequest, LogTag {

    override var callback: Callback? = null

    constructor(mo: RemoteMusicObject, context: Context, cb: Callback) : this(mo, context) {
        callback = cb
    }

    override fun start() {
        val queue = Volley.newRequestQueue(context)
        val getTitle = object : StringRequest(Method.HEAD, mo.link,
            { response ->
                Log.d(LOGTAG, "starting getting title")
                Log.d(LOGTAG, "got title: $response")
                mo.title = response
                callback?.onSuccess()
            },
            { error ->
                Log.e(LOGTAG, "some volley error while taking title: $error")
                callback?.onError()
            }) {
            override fun parseNetworkResponse(response: NetworkResponse?): Response<String> {
                try {
                    val disposition = response!!.headers!!.get("Content-Disposition")
                    val name = disposition!!.substring(disposition.indexOf('"') + 1, disposition.length - 1)
                    return Response.success<String>(name, HttpHeaderParser.parseCacheHeaders(response))
                } catch (ignored: Exception) {
                    return Response.error(VolleyError("parsing headers error"))
                }
            }
        }

        getTitle.retryPolicy = DefaultRetryPolicy(
            DURATION, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        queue.add(getTitle)
    }

    companion object {
        private val DURATION = 60000 * 100
    }

}

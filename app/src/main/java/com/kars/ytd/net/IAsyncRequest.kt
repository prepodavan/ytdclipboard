package com.kars.ytd.net

import com.kars.ytd.Callback

interface IAsyncRequest {

    var callback: Callback?
    fun start()

}

package com.kars.ytd.net

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import com.kars.ytd.Callback
import com.kars.ytd.LogTag
import com.kars.ytd.RemoteMusicObject
import com.kars.ytd.notification.LocalNotificationManager

class Downloader(private val musicObject: RemoteMusicObject, private val context: Context) : IAsyncRequest, LogTag {

    override var callback: Callback? = null

    constructor(musicObject: RemoteMusicObject, context: Context, cb: Callback) : this(musicObject, context) {
        callback = cb
    }

    override fun start() {
        startLog()
        val dm = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        LocalNotificationManager.getInstance(context).cancel()
        dm.enqueue(
            DownloadManager.Request(Uri.parse(musicObject.link))
                .setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE
                )
                .setAllowedOverRoaming(false)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_MUSIC, musicObject.title)
        )
    }

}

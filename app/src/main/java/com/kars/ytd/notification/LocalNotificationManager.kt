package com.kars.ytd.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import com.kars.ytd.ACTION_RETRY
import com.kars.ytd.LogTag
import com.kars.ytd.R
import com.kars.ytd.receivers.OnRetryReceiver

import java.io.File

class LocalNotificationManager private constructor(private val nm: NotificationManager) : LogTag {


    fun start(context: Context) {
        nm.notify(
            context.getString(R.string.clip_monitor_service), ID,
            NotificationCompat.Builder(context, channelID)
                .setSmallIcon(R.drawable.ic_notif)
                .setContentText("Downloading video...")
                .build()
        )
    }

    fun complete(context: Context) {
        val externalPath = Environment.getExternalStorageDirectory().path
        val path = "$externalPath/${Environment.DIRECTORY_MUSIC}/${getTitle(context)}"

        val intent = Intent()
            .setAction(Intent.ACTION_VIEW)
            .setDataAndType(Uri.fromFile(File(path)), "audio/*")

        nm.notify(
            context.getString(R.string.clip_monitor_service), ID,
            NotificationCompat.Builder(context, channelID)
                .setAutoCancel(true)
                .setContentText("Completed ${getTitle(context)}")
                .setSmallIcon(R.drawable.ic_notif)
                .setContentIntent(PendingIntent.getActivity(context, 0, intent, 0))
                .build()
        )
    }

    fun error(context: Context, link: String) {

        val retryIntent = Intent(context, OnRetryReceiver::class.java).apply {
            action = ACTION_RETRY
            putExtra("link", link)
        }

        val retryPendingIntent = PendingIntent.getBroadcast(context, 0, retryIntent, 0)

        nm.notify(
            context.getString(R.string.clip_monitor_service), ID,
            NotificationCompat.Builder(context, channelID)
                .setAutoCancel(true)
                .setContentText("Error with ${getTitle(context)}")
                .setSmallIcon(R.drawable.ic_notif)
                .addAction(R.drawable.ic_notif, context.getString(R.string.retryButtonText), retryPendingIntent)
                .build()
        )
    }

    private fun getTitle(context: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString("title", "music.mp3")
    }

    fun cancel() {
        nm.cancel(ID)
    }

    companion object {

        private val ID = 5077
        private val channelID = "ytd"

        fun getInstance(context: Context): LocalNotificationManager {
            val nm = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            return LocalNotificationManager(nm)
        }
    }

}

package com.kars.ytd

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class RemoteMusicObject {

    val yid: String

    val context: Context

    var title: String = "some-music.mp3"
        get() {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            field = prefs.getString("title", field)
            return field
        }
        set(value) {
            field = value
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString("title", field)
            editor.apply()
        }

    val link: String
        get() = "http://$domain/$yid"

    val domain: String
        get() {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getString("domain", context!!.getString(R.string.domain))
        }

    constructor(context: Context, yid: String) {
        this.yid = yid
        this.context = context
    }

    constructor(context: Context, yid: String, title: String) {
        this.context = context
        this.yid = yid
        this.title = title
    }

}

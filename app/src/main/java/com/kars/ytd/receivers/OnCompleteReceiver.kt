package com.kars.ytd.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.kars.ytd.*
import com.kars.ytd.notification.LocalNotificationManager
import com.kars.ytd.service.RequestManager


class OnCompleteReceiver : BroadcastReceiver(), LogTag {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(LOGTAG, "received complete from DM")
        LocalNotificationManager.getInstance(context).complete(context)
        context.stopService(Intent(context, RequestManager::class.java))
    }
}
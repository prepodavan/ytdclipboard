package com.kars.ytd.receivers

import com.kars.ytd.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.kars.ytd.service.ClipboardMonitor


class ClipboardMonitorStarter : BroadcastReceiver(), LogTag {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            val service = context.startService(Intent(context, ClipboardMonitor::class.java))
            Log.d(LOGTAG, "trying to start service")
            if (service == null) {
                Log.e(LOGTAG, "Can't start service ${ClipboardMonitor::javaClass.name}")
            }
        } else {
            Log.e(LOGTAG, "Received unexpected intent $intent")
        }
    }
}

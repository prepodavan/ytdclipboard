package com.kars.ytd.receivers
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.kars.ytd.*
import com.kars.ytd.service.RequestManager


class OnRetryReceiver : BroadcastReceiver(), LogTag {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(LOGTAG, "received error in downloading")
        context.startService(Intent(context, RequestManager::class.java).apply {
            putExtra("link", intent.getStringExtra("link"))
        })
    }
}
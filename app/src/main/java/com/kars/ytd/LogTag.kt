package com.kars.ytd

import android.util.Log

interface LogTag {

    val LOGTAG: String
        get() = this.javaClass.name

    fun startLog() {
        Log.d(LOGTAG, "started")
    }

    fun destroyedLog() {
        Log.d(LOGTAG, "destroyed")
    }

}
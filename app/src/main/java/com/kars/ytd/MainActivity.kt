package com.kars.ytd

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.kars.ytd.service.ClipboardMonitor

class MainActivity : Activity(), LogTag {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!ClipboardMonitor.isRunning)
            startService(Intent(this, ClipboardMonitor::class.java))
        startLog()
        finish()
    }
}
package com.kars.ytd.service

import android.app.DownloadManager
import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log
import com.kars.ytd.*
import com.kars.ytd.net.Downloader
import com.kars.ytd.net.PrepareMusicRequest
import com.kars.ytd.notification.LocalNotificationManager
import com.kars.ytd.receivers.OnCompleteReceiver


class RequestManager : Service(), LogTag, Callback {

    private lateinit var yid: String
    private lateinit var link: String
    private lateinit var receiver: OnCompleteReceiver
    private lateinit var musicObject: RemoteMusicObject

    override fun onBind(intent: Intent): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        receiver = OnCompleteReceiver()
        registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        startLog()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
        destroyedLog()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent ?: return START_STICKY
        link = intent.getStringExtra("link")
        yid = getYID(link)
        musicObject = RemoteMusicObject(applicationContext, yid)
        Log.d(LOGTAG, "received $yid to download")
        LocalNotificationManager.getInstance(applicationContext).start(applicationContext)
        PrepareMusicRequest(musicObject, applicationContext, this).start()
        return START_STICKY
    }

    private fun getYID(link: String): String {
        return link.substring(link.lastIndexOf(if (link.contains("watch")) '=' else '/') + 1)
    }

    override fun onSuccess() {
        val downloader = Downloader(musicObject, applicationContext)
        downloader.start()
    }

    override fun onError() {
        val context = applicationContext
        LocalNotificationManager.getInstance(context).error(context, link)
        stopSelf()
    }

}
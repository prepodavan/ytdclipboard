package com.kars.ytd.service

import android.preference.PreferenceManager
import com.kars.ytd.*
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.IBinder
import android.content.ClipboardManager
import android.util.Log
import java.lang.Exception
import java.util.regex.Pattern


class ClipboardMonitor : Service(), LogTag {

    private lateinit var cm: ClipboardManager
    private lateinit var task: MonitorTask
    private lateinit var prefs: SharedPreferences

    override fun onBind(intent: Intent): IBinder? = null

    override fun onDestroy() {
        super.onDestroy()
        task.cancel()
        isRunning = false
        destroyedLog()
    }

    override fun onCreate() {
        super.onCreate()
        cm  = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        task = MonitorTask()
        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        staticPrefs = prefs
        task.start()
        isRunning = true
        startLog()
    }

    private inner class MonitorTask internal constructor() : Thread("ClipboardMonitor") {

        private var keepRunning = false
        private var oldClip: String? = null

        fun cancel() {
            keepRunning = false
        }

        override fun run() {
            Log.d(LOGTAG, "thread is running")
            keepRunning = true
            while (keepRunning) {
                try {
                    doTask()
                    sleep(prefs.getInt(KEY_MONITOR_INTERVAL, DEF_MONITOR_INTERVAL).toLong())
                } catch (e: Exception) {
                    Log.e(LOGTAG, "some exception while doing task or sleep", e)
                }
            }
        }

        private fun doTask() {
            val newClip = cm.primaryClip?.getItemAt(0)?.text.toString()
            if (newClip != oldClip && isYTLink(newClip)) {
                onDetect(newClip)
            }
        }

        private fun onDetect(clip: String) {
            Log.d(LOGTAG, "detect new text clip: $clip")
            save(clip)
            startDownloading(clip)
        }

        private fun save(clip: String) {
            oldClip = clip
        }

        private fun startDownloading(link: String) {
            startService(Intent(applicationContext, RequestManager::class.java).apply {
                putExtra("link", link)
            })
        }

        private fun isYTLink(s: String): Boolean {
            val basic = Pattern.compile("^(http:://|https://)?(www\\.)?youtube\\.com/watch\\?v=[\\w-]+$")
            val share = Pattern.compile("^(http:://|https://)?(www\\.)?youtu\\.be/[\\w-]+$")
            return share.matcher(s).matches() || basic.matcher(s).matches()
        }
    }

    companion object {
        private val runningString = "isRMRunning"
        private const val KEY_MONITOR_INTERVAL = "monitor.interval"
        private const val DEF_MONITOR_INTERVAL = 3000
        private var staticPrefs: SharedPreferences? = null

        var isRunning: Boolean
            get() = staticPrefs?.getBoolean(runningString, false) ?: false
            private set(v) {
                val editor = staticPrefs?.edit()
                editor?.putBoolean(runningString, v)
                editor?.apply()
            }
    }

}